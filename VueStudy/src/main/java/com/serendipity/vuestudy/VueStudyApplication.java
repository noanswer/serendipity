package com.serendipity.vuestudy;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class VueStudyApplication {

    public static void main(String[] args) {
        SpringApplication.run(VueStudyApplication.class, args);
    }

}
