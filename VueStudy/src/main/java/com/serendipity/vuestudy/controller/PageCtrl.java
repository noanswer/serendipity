package com.serendipity.vuestudy.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

@Controller
public class PageCtrl {

    // @Controller 에서먼 String return 으로 페이지 넘김
    @RequestMapping(value = "/lastword")
    public String lastwordCtrl() {
        return "/view/lastword.html";
    }

    @RequestMapping(value = "/baseball")
    public String numberBaseballCtrl() {
        return "/view/baseball.html";
    }

    private static final Logger log = LoggerFactory.getLogger(PageCtrl.class);
}