package com.serendipity.vuestudy.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

@RestController
public class MainCtrl {

    @RequestMapping(value = "/")
    public void mainCtrl(HttpServletRequest request, HttpServletResponse response) throws IOException {
        log.info("f" + request.getContextPath().toString());
        response.sendRedirect(request.getContextPath() + "/index.html");
    }

    @RequestMapping(value = "/info")
    public Map<String, Object> getInfo(){
        Map<String, Object> map = new HashMap<>();
        map.put("info", "test");
        return map;
    }

    private static final Logger log = LoggerFactory.getLogger(MainCtrl.class);
}